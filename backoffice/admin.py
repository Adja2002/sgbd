import sys
import sqlite3
from PySide6.QtWidgets import QApplication, QMainWindow, QWidget, QVBoxLayout, QLabel, QLineEdit, QPushButton, QMessageBox, QDialog, QListWidget, QListWidgetItem
from PySide6.QtGui import QColor, QFont
from PySide6.QtCore import Qt, Signal, Slot
import re

class AdminPage(QMainWindow):
    show_accounts_signal = Signal()

    def __init__(self):
        super(AdminPage, self).__init__()
        self.setWindowTitle("Page d'Administration")
        self.setGeometry(100, 100, 400, 300)  # Définir la taille de la fenêtre

        # Création du widget principal
        self.main_widget = QWidget()
        self.setCentralWidget(self.main_widget)

        # Couleur de fond bleu clair
        self.main_widget.setAutoFillBackground(True)
        palette = self.main_widget.palette()
        palette.setColor(self.main_widget.backgroundRole(), QColor(173, 216, 230))  # Couleur bleu clair
        self.main_widget.setPalette(palette)

        # Layout principal
        self.layout = QVBoxLayout()
        self.main_widget.setLayout(self.layout)

        # Titre "Bienvenue"
        self.welcome_label = QLabel("Bienvenue")
        self.welcome_label.setAlignment(Qt.AlignCenter)  # Utilisez la classe Qt ici
        self.welcome_label.setFont(QFont('Arial', 20))  # Augmenter la taille de la police
        self.layout.addWidget(self.welcome_label)

        # Bouton de création de compte
        self.create_account_button = QPushButton("Créer un compte")
        self.create_account_button.setStyleSheet("QPushButton { background-color: white; color: black; border: 1px solid black; padding: 5px 10px; border-radius: 5px; }")
        self.create_account_button.clicked.connect(self.create_account)
        self.layout.addWidget(self.create_account_button, alignment=Qt.AlignCenter)  # Utilisez la classe Qt ici

        # Bouton "Afficher comptes"
        self.show_accounts_button = QPushButton("Afficher comptes")
        self.show_accounts_button.setStyleSheet("QPushButton { background-color: white; color: black; border: 1px solid black; padding: 5px 10px; border-radius: 5px; }")
        self.show_accounts_button.clicked.connect(self.show_accounts_signal.emit)
        self.layout.addWidget(self.show_accounts_button, alignment=Qt.AlignCenter)  # Utilisez la classe Qt ici

        # Création de la base de données et de la table des utilisateurs
        self.create_database()

    def create_database(self):
        # Connexion à la base de données SQLite
        self.connection = sqlite3.connect("users.db")
        self.cursor = self.connection.cursor()

        # Création de la table des utilisateurs si elle n'existe pas
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS users (
                                    id INTEGER PRIMARY KEY,
                                    username TEXT,
                                    password TEXT,
                                    role TEXT
                                )''')
        self.connection.commit()

    def create_account(self):
        # Interface pour saisir les informations du compte
        dialog = QDialog(self)
        dialog.setWindowTitle("Créer un compte")
        layout = QVBoxLayout()

        username_label = QLabel("Nom d'utilisateur:")
        self.username_input = QLineEdit()
        layout.addWidget(username_label)
        layout.addWidget(self.username_input)

        password_label = QLabel("Mot de passe:")
        self.password_input = QLineEdit()
        self.password_input.setEchoMode(QLineEdit.Password)
        layout.addWidget(password_label)
        layout.addWidget(self.password_input)

        role_label = QLabel("Rôle:")
        self.role_input = QLineEdit()
        layout.addWidget(role_label)
        layout.addWidget(self.role_input)

        create_button = QPushButton("Créer")
        create_button.clicked.connect(self.create_user)
        layout.addWidget(create_button)

        dialog.setLayout(layout)
        dialog.exec()

    def create_user(self):
        # Récupérer les informations du compte
        username = self.username_input.text()
        password = self.password_input.text()
        role = self.role_input.text()

        # Validation des données
        if not re.match(r"[^@]+@esp\.sn", username):
            QMessageBox.warning(self, "Créer un compte", "Le nom d'utilisateur doit être sous le format nom_utilisateur@esp.sn.")
            return

        if username and password and role:
            # Insérer l'utilisateur dans la base de données
            self.cursor.execute("INSERT INTO users (username, password, role) VALUES (?, ?, ?)", (username, password, role))
            self.connection.commit()

            # Logique pour créer un compte (vous pouvez remplacer ceci par votre propre logique de création de compte)
            QMessageBox.information(self, "Créer un compte", f"Compte créé avec succès.\nNom d'utilisateur: {username}, Rôle: {role}")
        else:
            QMessageBox.warning(self, "Créer un compte", "Veuillez remplir tous les champs.")

class ShowAccountsPage(QWidget):
    def __init__(self, accounts):
        super(ShowAccountsPage, self).__init__()

        layout = QVBoxLayout()
        self.setLayout(layout)

        label = QLabel("Liste des comptes créés par l'administrateur:")
        layout.addWidget(label)

        # Liste des comptes
        self.accounts_list = QListWidget()
        for account in accounts:
            item = QListWidgetItem(f"Nom d'utilisateur: {account[0]}, Rôle: {account[1]}")
            self.accounts_list.addItem(item)
        layout.addWidget(self.accounts_list)

        # Bouton "Supprimer compte"
        self.delete_account_button = QPushButton("Supprimer compte")
        self.delete_account_button.clicked.connect(self.delete_account)
        layout.addWidget(self.delete_account_button)

    @Slot()
    def delete_account(self):
        selected_item = self.accounts_list.currentItem()
        if selected_item:
            account_info = selected_item.text()
            username = account_info.split(":")[1].strip().split(",")[0].strip()
            if QMessageBox.question(self, "Supprimer un compte", f"Voulez-vous vraiment supprimer le compte pour l'utilisateur '{username}' ?", QMessageBox.Yes | QMessageBox.No) == QMessageBox.Yes:
                # Supprimer le compte de la base de données
                connection = sqlite3.connect("users.db")
                cursor = connection.cursor()
                cursor.execute("DELETE FROM users WHERE username = ?", (username,))
                connection.commit()
                connection.close()

                # Actualiser la liste des comptes
                self.update_accounts_list()

    def update_accounts_list(self):
        self.accounts_list.clear()
        accounts = fetch_accounts()
        for account in accounts:
            item = QListWidgetItem(f"Nom d'utilisateur: {account[0]}, Rôle: {account[1]}")
            self.accounts_list.addItem(item)

def fetch_accounts():
    # Connexion à la base de données SQLite
    connection = sqlite3.connect("users.db")
    cursor = connection.cursor()

    # Récupérer tous les comptes depuis la base de données
    cursor.execute("SELECT username, role FROM users")
    accounts = cursor.fetchall()
    connection.close()

    return accounts

if __name__ == "__main__":
    app = QApplication(sys.argv)
    admin_page = AdminPage()
    show_accounts_page = ShowAccountsPage(accounts=fetch_accounts())  # Récupérer les comptes depuis la base de données
    admin_page.show_accounts_signal.connect(show_accounts_page.show)
    admin_page.show()
    sys.exit(app.exec())
