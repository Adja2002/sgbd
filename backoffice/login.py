import sys
from PySide6.QtCore import Qt
from PySide6.QtWidgets import QApplication, QMainWindow, QWidget, QVBoxLayout, QHBoxLayout, QPushButton, QMessageBox, QLabel, QSpacerItem, QSizePolicy, QStackedWidget, QTextEdit

from log import CheckTeachingsWindow

class ResponsibleHomePage(QMainWindow):
    def __init__(self):
        super(ResponsibleHomePage, self).__init__()
        self.setWindowTitle("Page d'accueil du responsable pédagogique")
        self.setGeometry(100, 100, 600, 400)

        # Appliquer un style CSS pour définir l'arrière-plan en bleu ciel et le texte en blanc
        self.setStyleSheet("background-color: #b3e0ff; color: white;")

        # Ajouter un widget central pour organiser les éléments
        central_widget = QWidget(self)
        self.setCentralWidget(central_widget)

        main_layout = QVBoxLayout(central_widget)

        # Bouton de déconnexion en haut à gauche
        self.logout_button = QPushButton("Se déconnecter")
        self.logout_button.setStyleSheet("background-color: #1e90ff; color: white; font-size: 12px; padding: 5px 10px;")
        self.logout_button.clicked.connect(self.logout)
        main_layout.addWidget(self.logout_button, alignment=Qt.AlignLeft)

        # Ajouter un espace vide entre le bouton de déconnexion et le message de bienvenue
        main_layout.addSpacerItem(QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding))

        # Message de bienvenue
        welcome_label = QLabel("Bienvenue dans votre espace!")
        welcome_label.setAlignment(Qt.AlignCenter)
        welcome_label.setStyleSheet("font-size: 20px;")
        main_layout.addWidget(welcome_label)

        # Ajouter un espace vide entre le message de bienvenue et les fonctionnalités
        main_layout.addSpacerItem(QSpacerItem(30, 50, QSizePolicy.Minimum, QSizePolicy.Expanding))

        # Layout horizontal pour les fonctionnalités
        functionalities_layout = QHBoxLayout()
        main_layout.addLayout(functionalities_layout)
        main_layout.addStretch(1)

        # Ajouter les boutons de fonctionnalités
        functionalities_layout.addWidget(self.create_functionality_button("Vérifier enseignements", self.check_teachings))
        functionalities_layout.addWidget(self.create_functionality_button("Exploiter fiches d'évaluation", self.exploit_evaluation_sheets))
        functionalities_layout.setAlignment(Qt.AlignCenter)  # Aligner les fonctionnalités au centre
        # Bouton pour vérifier les enseignements
        check_teachings_button = QPushButton("Vérifier enseignements")
        check_teachings_button.clicked.connect(self.show_teachings_page)
        layout.addWidget(check_teachings_button) # type: ignore
         # Page du cahier de textes virtuel
        self.teachings_page = CheckTeachingsWindow()
        self.stacked_widget.addWidget(self.teachings_page)

    def show_teachings_page(self):
        self.stacked_widget.setCurrentWidget(self.teachings_page)

    def create_functionality_button(self, label, function):
        button = QPushButton(label)
        button.setStyleSheet("background-color: #1e90ff; color: white; font-size: 14px;")
        button.clicked.connect(function)
        return button

    def check_teachings(self):
        # Implémentez la logique pour vérifier les enseignements
        # Zone de texte pour afficher les enseignements
        self.teachings_textedit = QTextEdit()
        layout.addWidget(self.teachings_textedit) # type: ignore

        # Exemple de texte pour le cahier de textes
        teachings_info = """
        1. Matière: Recherche Opérationnelle
           Date: 10/02/2024
           Heure : 14h:30 - 16h:30
           Professeur : Chérif TOURE 

        2. Matière: SGBD
           Date: 12/03/2024
           Heure : 16h:30 - 18h:30
           Professeur : Cheikh Amath MBACKE
           
        3. Matière: Algorithme et Structures de données
           Date: 05/01/2024
           Heure : 08h:00 - 10h:00
           Professeur : Ibrahima GAYE
           
        4. Matière: Systèmes d'exploitation
           Date: 03/12/2023
           Heure : 10h:00 - 12h:00
           Professeur : Mandicou BA
           
        """
        self.teachings_textedit.setPlainText(teachings_info)

         #QMessageBox.information(self, "Vérifier enseignements", "Fonctionnalité non implémentée.")

    def exploit_evaluation_sheets(self):
        # Implémentez la logique pour exploiter les fiches d'évaluation
        QMessageBox.information(self, "Exploiter fiches d'évaluation", "Fonctionnalité non implémentée.")

    def logout(self):
        # Fermer la fenêtre actuelle (page d'accueil)
        self.close()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    home_page = ResponsibleHomePage()
    home_page.show()
    sys.exit(app.exec())

