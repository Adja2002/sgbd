import sys
import sqlite3
from PySide6.QtWidgets import QApplication, QMainWindow, QWidget, QVBoxLayout, QLabel, QLineEdit, QPushButton, QMessageBox
from PySide6.QtGui import QColor, QFont
from PySide6.QtCore import Qt

class LoginPage(QMainWindow):
    def __init__(self):
        super(LoginPage, self).__init__()
        self.setWindowTitle("Page de Connexion")
        self.setGeometry(100, 100, 400, 300)  # Définir la taille de la fenêtre

        # Création du widget principal
        self.main_widget = QWidget()
        self.setCentralWidget(self.main_widget)

        # Couleur de fond bleu clair
        self.main_widget.setAutoFillBackground(True)
        palette = self.main_widget.palette()
        palette.setColor(self.main_widget.backgroundRole(), QColor(173, 216, 230))  # Couleur bleu clair
        self.main_widget.setPalette(palette)

        # Layout principal
        self.layout = QVBoxLayout()
        self.main_widget.setLayout(self.layout)

        # Titre "Connexion"
        self.login_label = QLabel("Connexion")
        self.login_label.setAlignment(Qt.AlignCenter)  # Utilisez la classe Qt ici
        self.login_label.setFont(QFont('Arial', 20))  # Augmenter la taille de la police
        self.layout.addWidget(self.login_label)

        # Champ de saisie pour le nom d'utilisateur
        self.username_label = QLabel("Nom d'utilisateur:")
        self.layout.addWidget(self.username_label)
        self.username_input = QLineEdit()
        self.layout.addWidget(self.username_input)

        # Champ de saisie pour le mot de passe
        self.password_label = QLabel("Mot de passe:")
        self.layout.addWidget(self.password_label)
        self.password_input = QLineEdit()
        self.password_input.setEchoMode(QLineEdit.Password)
        self.layout.addWidget(self.password_input)

        # Bouton de connexion
        self.login_button = QPushButton("Se connecter")
        self.login_button.clicked.connect(self.login)
        self.layout.addWidget(self.login_button, alignment=Qt.AlignCenter)  # Utilisez la classe Qt ici

    def login(self):
        # Récupérer les informations de connexion
        username = self.username_input.text()
        password = self.password_input.text()

        # Vérifier les identifiants dans la base de données
        connection = sqlite3.connect("users.db")
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM users WHERE username = ? AND password = ?", (username, password))
        user = cursor.fetchone()
        connection.close()

        if user:
            # Vérifier si l'utilisateur est un responsable pédagogique
            if user[3] == "responsable_pédagogique":  # Supposant que le rôle est stocké dans le quatrième champ de la table
                QMessageBox.information(self, "Connexion", "Connexion réussie en tant que responsable pédagogique.")
                self.load_responsable_page()  # Rediriger vers la page d'accueil du responsable pédagogique
            else:
                QMessageBox.warning(self, "Connexion", "Vous n'avez pas les autorisations nécessaires pour accéder à cette interface.")
        else:
            QMessageBox.warning(self, "Connexion", "Nom d'utilisateur ou mot de passe incorrect.")

    def load_responsable_page(self):
        # Implémentez ici la logique pour charger la page d'accueil du responsable pédagogique
        responsable_page = QWidget()
        responsable_page_layout = QVBoxLayout()
        responsable_page.setLayout(responsable_page_layout)
        label = QLabel("Page d'accueil du responsable pédagogique")
        responsable_page_layout.addWidget(label)
        self.setCentralWidget(responsable_page)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    login_page = LoginPage()
    login_page.show()
    sys.exit(app.exec())

