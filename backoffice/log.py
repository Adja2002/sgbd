import sys
from PySide6.QtWidgets import QApplication, QMainWindow, QWidget, QVBoxLayout, QPushButton, QStackedWidget, QTextEdit

class CheckTeachingsWindow(QWidget):
    def __init__(self):
        super(CheckTeachingsWindow, self).__init__()
        self.setWindowTitle("Cahier de textes virtuel")
        self.setGeometry(100, 100, 600, 400)

        layout = QVBoxLayout(self)

        # Zone de texte pour afficher les enseignements
        self.teachings_textedit = QTextEdit()
        layout.addWidget(self.teachings_textedit)

        # Exemple de texte pour le cahier de textes
        teachings_info = """
        1. Matière: Recherche Opérationnelle
           Date: 10/02/2024
           Heure : 14h:30 - 16h:30
           Professeur : Chérif TOURE 

        2. Matière: SGBD
           Date: 12/03/2024
           Heure : 16h:30 - 18h:30
           Professeur : Cheikh Amath MBACKE
           
        3. Matière: Algorithme et Structures de données
           Date: 05/01/2024
           Heure : 08h:00 - 10h:00
           Professeur : Ibrahima GAYE
           
        4. Matière: Systèmes d'exploitation
           Date: 03/12/2023
           Heure : 10h:00 - 12h:00
           Professeur : Mandicou BA
           
        """
        self.teachings_textedit.setPlainText(teachings_info)

class ResponsibleHomePage(QMainWindow):
    def __init__(self):
        super(ResponsibleHomePage, self).__init__()
        self.setWindowTitle("Page d'accueil du responsable pédagogique")
        self.setGeometry(100, 100, 600, 400)

        self.stacked_widget = QStackedWidget()
        self.setCentralWidget(self.stacked_widget)

        # Page d'accueil
        self.home_page = QWidget()
        layout = QVBoxLayout(self.home_page)

        # Bouton pour vérifier les enseignements
        check_teachings_button = QPushButton("Vérifier enseignements")
        check_teachings_button.clicked.connect(self.show_teachings_page)
        layout.addWidget(check_teachings_button)

        # Bouton pour exploiter les fiches d'évaluation
        exploit_evaluation_sheets_button = QPushButton("Exploiter fiches d'évaluation")
        exploit_evaluation_sheets_button.clicked.connect(self.exploit_evaluation_sheets)
        layout.addWidget(exploit_evaluation_sheets_button)

        self.stacked_widget.addWidget(self.home_page)

        # Page du cahier de textes virtuel
        self.teachings_page = CheckTeachingsWindow()
        self.stacked_widget.addWidget(self.teachings_page)

    def show_teachings_page(self):
        self.stacked_widget.setCurrentWidget(self.teachings_page)

    def exploit_evaluation_sheets(self):
        # Implémentez la logique pour exploiter les fiches d'évaluation
        pass

if __name__ == "__main__":
    app = QApplication(sys.argv)
    home_page = ResponsibleHomePage()
    home_page.show()
    sys.exit(app.exec())
