import sys
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QVBoxLayout, QHBoxLayout, QPushButton, QDialog, QTableWidget, QTableWidgetItem
from PyQt5.QtGui import QPixmap, QIcon
import mysql.connector

class MainPage(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Main Page')
        self.setStyleSheet('''
            QWidget {
                background-color: #E0F2F1; /* Couleur de fond */
            }
            QLabel#username_label {
                font-size: 14px;
                color: black;
            }
            QPushButton {
                border: none; /* Supprimer la bordure */
                padding: 0; /* Supprimer le padding */
                font-size: 18px; /* Taille de la police */
                color: black; /* Couleur du texte en noir */
                text-align: center; /* Alignement du texte au centre */
                background-color: rgba(33, 150, 243, 0.5); /* Couleur de fond avec transparence */
                border-radius: 10px; /* Bordure arrondie */
            }
            QPushButton:hover {
                background-color: rgba(0, 0, 0, 0.3); /* Couleur de fond au survol */
            }
            QPushButton::icon {
                /* Ajuster la position de l'icône */
                padding-bottom: 10px; /* Espacement en bas de l'icône */
            }
        ''')

        # Logo
        self.logo_label = QLabel()
        self.logo_pixmap = QPixmap('th.JPEG')  # Chemin vers votre fichier logo
        self.logo_pixmap = self.logo_pixmap.scaled(50, 50)  # Redimensionner le logo
        self.logo_label.setPixmap(self.logo_pixmap)

        # Username
        self.username_label = QLabel("Utilisateur: Mr.Fall")  # Remplacez "John Doe" par le nom de l'utilisateur connecté

        # Bouton 1
        self.button1 = QPushButton()
        self.set_button_icon_text(self.button1, 'Déroulement Enseignement', 'th(3).JPEG')
        self.button1.clicked.connect(self.show_table_dialog)

        # Bouton 2
        self.button2 = QPushButton()
        self.set_button_icon_text(self.button2, 'PV et Rapport', 'th(2).JPEG')

        # Layout
        logo_layout = QHBoxLayout()
        logo_layout.addWidget(self.logo_label)
        logo_layout.addStretch()
        logo_layout.addWidget(self.username_label)

        buttons_layout = QHBoxLayout()
        buttons_layout.addWidget(self.button1)
        buttons_layout.addWidget(self.button2)

        main_layout = QVBoxLayout()
        main_layout.addLayout(logo_layout)
        main_layout.addStretch(1)
        main_layout.addLayout(buttons_layout)
        main_layout.addStretch(1)
        self.setLayout(main_layout)

    def set_button_icon_text(self, button, text, icon_path):
        pixmap = QPixmap(icon_path)
        button.setIcon(QIcon(pixmap))
        button.setIconSize(pixmap.rect().size())
        button.setText(text)

    def show_table_dialog(self):
        dialog = TableDialog(self)
        dialog.exec_()

from PyQt5.QtWidgets import QTableWidgetItem, QHeaderView

class TableDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle('Déroulement Enseignement')
        self.resize(400, 300)

        # Tableau pour afficher les classes disponibles
        self.class_table_widget = QTableWidget()
        self.class_table_widget.setColumnCount(1)  # Une seule colonne pour le nom de la classe
        self.class_table_widget.setHorizontalHeaderLabels(["Classes disponibles"])
        self.populate_class_table()

        # Tableau pour afficher les détails de la classe sélectionnée
        self.details_table_widget = QTableWidget()
        self.details_table_widget.setColumnCount(5)  # Vous pouvez ajuster le nombre de colonnes selon vos besoins
        self.details_table_widget.setHorizontalHeaderLabels(["ID", "Cours", "Professeur", "Date", "Présent/Absent"])

        layout = QHBoxLayout()
        layout.addWidget(self.class_table_widget)
        layout.addWidget(self.details_table_widget)
        self.setLayout(layout)

        # Ajout de la fonctionnalité de clic à la table des classes disponibles
        self.class_table_widget.cellClicked.connect(self.show_class_details)

    def populate_class_table(self):
        # Connexion à la base de données
        connection = mysql.connector.connect(
            host='localhost',
            user='root',
            password='',
            database='back'
        )
        cursor = connection.cursor()

        # Exécuter une requête pour récupérer les classes disponibles
        cursor.execute("SELECT DISTINCT classe FROM enseignement")
        classes = cursor.fetchall()

        # Fermer la connexion à la base de données
        connection.close()

        # Remplir le tableau des classes disponibles
        self.class_table_widget.setRowCount(len(classes))
        for row, row_data in enumerate(classes):
            item = QTableWidgetItem(str(row_data[0]))
            self.class_table_widget.setItem(row, 0, item)

    def show_class_details(self, row, column):
        # Récupérer le nom de la classe sélectionnée
        selected_class = self.class_table_widget.item(row, column).text()

        # Connexion à la base de données
        connection = mysql.connector.connect(
            host='localhost',
            user='root',
            password='',
            database='back'
        )
        cursor = connection.cursor()

        # Exécuter une requête pour récupérer les détails de la classe sélectionnée
        cursor.execute("SELECT * FROM enseignement WHERE classe = %s", (selected_class,))
        class_details = cursor.fetchall()

        # Fermer la connexion à la base de données
        connection.close()

        # Remplir le tableau des détails de la classe sélectionnée
        self.details_table_widget.setRowCount(len(class_details))
        for row, row_data in enumerate(class_details):
            for col, col_data in enumerate(row_data):
                item = QTableWidgetItem(str(col_data))
                self.details_table_widget.setItem(row, col, item)

        # Redimensionner automatiquement les colonnes pour afficher correctement les données
        self.details_table_widget.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    main_page = MainPage()
    main_page.show()
    sys.exit(app.exec_())
