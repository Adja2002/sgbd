import sys
import pymysql

from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QLineEdit, QPushButton, QVBoxLayout, QMessageBox
from PyQt5.QtCore import Qt
import home

class LoginPage(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Login Page')
        self.resize(300, 150)
        self.setStyleSheet("background-color: #E0F2F1;")  # Définir la couleur de fond

        # Widgets
        self.label_username = QLabel('Email:')
        self.label_password = QLabel('Password:')
        self.input_username = QLineEdit()
        self.input_password = QLineEdit()
        self.input_password.setEchoMode(QLineEdit.Password)
        self.button_login = QPushButton('Login')
        self.button_login.clicked.connect(self.open_home_page)
        self.button_login.setStyleSheet('''
            QPushButton {
                background-color: #4CAF50;
                color: white;
                border: none;
                border-radius: 5px;
                padding: 10px 20px;
                font-size: 16px;
            }
            QPushButton:hover {
                background-color: #45a049;
            }
        ''')

        # Layout
        layout = QVBoxLayout()
        layout.addWidget(self.label_username)
        layout.addWidget(self.input_username)
        layout.addWidget(self.label_password)
        layout.addWidget(self.input_password)
        layout.addWidget(self.button_login)
        layout.setContentsMargins(50, 50, 50, 50)  # Définir les marges du layout
        self.setLayout(layout)

        # Signal connections
        self.button_login.clicked.connect(self.login)
    
    def open_home_page(self):
        self.home_page = home.MainPage()
        self.home_page.show()
        self.close()

    def login(self):
        email = self.input_username.text()
        mdp = self.input_password.text()
        print('Email:', email)
        print('Password:', mdp)
        conn = pymysql.connect(host='localhost', user='root', password='', database='back')
        
        try:
            with conn.cursor() as cursor:
                # Exécuter une requête SQL pour vérifier les informations de connexion
                sql = "SELECT * FROM ChefDep WHERE email = %s AND mdp = %s"
                cursor.execute(sql, (email, mdp))
                
                # Récupérer le résultat de la requête
                result = cursor.fetchone()
                
                # Vérifier si un utilisateur correspondant aux informations de connexion a été trouvé
                if email == 'ibrahimafall@esp.sn' and mdp == 'ibrahima':
                    QMessageBox.information(self, "Connexion réussie", "Connexion réussie !")
                    # Faire quelque chose après la connexion réussie, comme afficher une nouvelle fenêtre ou exécuter une autre action
                else:
                    QMessageBox.warning(self, "Erreur de connexion", "Adresse email ou mot de passe incorrect.")
        except pymysql.Error as e:
            QMessageBox.critical(self, "Erreur", f'Erreur lors de la connexion : {e}')
        finally:
            # Fermer la connexion
            conn.close()
            
if __name__ == '__main__':
    app = QApplication(sys.argv)
    login_page = LoginPage()
    login_page.show()
    sys.exit(app.exec_())
