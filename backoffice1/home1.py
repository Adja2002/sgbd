import sys
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QVBoxLayout, QHBoxLayout, QPushButton, QDialog, QTableWidget, QTableWidgetItem
from PyQt5.QtGui import QPixmap, QIcon
import mysql.connector

from PyQt5.QtWidgets import QLineEdit, QPushButton, QFileDialog
from PyQt5.QtWidgets import QTextEdit, QTabWidget
class PVFormDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle('Dresser PV')
        layout = QVBoxLayout()

        self.nom_line_edit = QLineEdit()
        layout.addWidget(self.nom_line_edit)

        self.import_button = QPushButton('Importer fichier')
        self.import_button.clicked.connect(self.import_file)
        layout.addWidget(self.import_button)

        self.save_button = QPushButton('Enregistrer')
        self.save_button.clicked.connect(self.insert_pv_data)
        layout.addWidget(self.save_button)


        self.setLayout(layout)
    def insert_pv_data(self):
        nom = self.nom_line_edit.text()
        # Obtenez d'autres données du formulaire, par exemple, le chemin du fichier importé

        # Connexion à la base de données
        connection = mysql.connector.connect(
            host='localhost',
            user='root',
            password='',
            database='back'
        )
        cursor = connection.cursor()

        # Exécutez la requête d'insertion avec les données saisies
        cursor.execute("INSERT INTO PV (nom, contenu, date) VALUES (%s, %s, CURRENT_TIMESTAMP())", (nom, 'contenu_du_fichier'))
        
        # Validez et enregistrez les modifications dans la base de données
        connection.commit()

        # Fermez la connexion à la base de données
        connection.close()

        # Affichez un message de confirmation
        print("Données insérées avec succès dans la base de données.")


    def import_file(self):
        file_dialog = QFileDialog()
        file_path, _ = file_dialog.getOpenFileName(self, 'Importer fichier', '', 'PDF Files (*.pdf);;Word Files (*.docx)')
        # Traitez ici le fichier sélectionné, par exemple, affichez son chemin dans la console
        print("Chemin du fichier sélectionné :", file_path)


class MainPage(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Main Page')
        self.setStyleSheet('''
            QWidget {
                background-color: #E0F2F1; /* Couleur de fond */
            }
            QLabel#username_label {
                font-size: 14px;
                color: black;
            }
            QPushButton {
                border: none; /* Supprimer la bordure */
                padding: 0; /* Supprimer le padding */
                font-size: 18px; /* Taille de la police */
                color: black; /* Couleur du texte en noir */
                text-align: center; /* Alignement du texte au centre */
                background-color: rgba(33, 150, 243, 0.5); /* Couleur de fond avec transparence */
                border-radius: 10px; /* Bordure arrondie */
            }
            QPushButton:hover {
                background-color: rgba(0, 0, 0, 0.3); /* Couleur de fond au survol */
            }
            QPushButton::icon {
                /* Ajuster la position de l'icône */
                padding-bottom: 10px; /* Espacement en bas de l'icône */
            }
        ''')

        # Logo
        self.logo_label = QLabel()
        self.logo_pixmap = QPixmap('th.JPEG')  # Chemin vers votre fichier logo
        self.logo_pixmap = self.logo_pixmap.scaled(50, 50)  # Redimensionner le logo
        self.logo_label.setPixmap(self.logo_pixmap)

        # Username
        self.username_label = QLabel("Utilisateur: Mlle.Camara")  # Remplacez "John Doe" par le nom de l'utilisateur connecté

        # Bouton "Dresser PV"
        self.button_pv = QPushButton()
        self.set_button_icon_text(self.button_pv, 'Liste des PV', 'th(5).JPEG')
        self.button_pv.clicked.connect(self.show_table_dialog)

        # Bouton "Liste des PV"
        self.button_list_pv = QPushButton()
        self.set_button_icon_text(self.button_list_pv, 'Dresser PV', 'th(4).JPEG')
        self.button_list_pv.clicked.connect(self.show_list_pv_dialog)
        self.button_list_pv.clicked.connect(self.show_pv_form_dialog)


        # Layout
        logo_layout = QHBoxLayout()
        logo_layout.addWidget(self.logo_label)
        logo_layout.addStretch()
        logo_layout.addWidget(self.username_label)

        buttons_layout = QHBoxLayout()
        buttons_layout.addWidget(self.button_pv)
        buttons_layout.addWidget(self.button_list_pv)

        main_layout = QVBoxLayout()
        main_layout.addLayout(logo_layout)
        main_layout.addStretch(1)
        main_layout.addLayout(buttons_layout)
        main_layout.addStretch(1)
        self.setLayout(main_layout)

    def show_pv_form_dialog(self):
        dialog = PVFormDialog(self)
        dialog.exec_()
        self.button_list_pv.clicked.connect(self.show_pv_form_dialog)



    def set_button_icon_text(self, button, text, icon_path):
        pixmap = QPixmap(icon_path)
        button.setIcon(QIcon(pixmap))
        button.setIconSize(pixmap.rect().size())
        button.setText(text)

    def show_table_dialog(self):
        dialog = TableDialog(self)
        dialog.exec_()

    def show_list_pv_dialog(self):
        # Ajoutez le code pour afficher la liste des PV ici
        pass



class TableDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle('Liste des PV')
        self.resize(400, 300)

        self.tab_widget = QTabWidget()
        self.populate_table()

        layout = QVBoxLayout()
        layout.addWidget(self.tab_widget)
        self.setLayout(layout)

        # Ajoutez une fonctionnalité de clic à votre tableau
        self.table_widget.cellClicked.connect(self.show_pv_content)

    def populate_table(self):
        # Connexion à la base de données
        connection = mysql.connector.connect(
            host='localhost',
            user='root',
            password='',
            database='back'
        )
        cursor = connection.cursor()

        # Exécuter une requête pour récupérer les données de la table enseignement
        cursor.execute("SELECT id, nom, date FROM PV")
        self.data = cursor.fetchall()  # Assignez les données à self.data

        # Fermer la connexion à la base de données
        connection.close()

        self.table_widget = QTableWidget()
        self.table_widget.setColumnCount(5)  # Ajouter une colonne pour l'ID
        self.table_widget.setHorizontalHeaderLabels(["Nom", "Date"])

        # Remplir le tableau avec les données de la base de données
        self.table_widget.setRowCount(len(self.data))
        for row, row_data in enumerate(self.data):
            for col, col_data in enumerate(row_data):
                item = QTableWidgetItem(str(col_data))
                self.table_widget.setItem(row, col, item)

        self.tab_widget.addTab(self.table_widget, "Liste des PV")

    def show_pv_content(self, row, column):
        # Récupérez le contenu du PV basé sur l'ID sélectionné
        pv_id = self.data[row][0]  # ID du PV sélectionné
        # Connexion à la base de données
        connection = mysql.connector.connect(
            host='localhost',
            user='root',
            password='',
            database='back'
        )
        cursor = connection.cursor()

        # Exécuter une requête pour récupérer le contenu du PV
        cursor.execute("SELECT contenu FROM PV WHERE id = %s", (pv_id,))
        pv_content = cursor.fetchone()[0]

        # Fermer la connexion à la base de données
        connection.close()

        text_edit = QTextEdit()
        
        text_edit.setPlainText(pv_content)
        self.tab_widget.addTab(text_edit, "Contenu du PV")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    main_page = MainPage()
    main_page.show()
    sys.exit(app.exec_())
